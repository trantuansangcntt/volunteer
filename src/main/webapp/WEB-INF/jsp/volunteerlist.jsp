<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml11.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Volunteer List</title>
</head>
<body>
	<h1>Volunteer List</h1>
	<table>
	<c:forEach var="volunteer" items="${volunteers}">
	<tr>
		<td>${volunteer.firstName}</td>
		<td>${volunteer.lastName}</td>
		<td><a href="volunteers/${volunteer.id}">detail</a></td>
		<td>
                	<form method="post" action="../volunteer/remove?volunteerId=${volunteer.id}">
                		<input type="submit" value="Remove" />
                	</form>
                </td>
	</tr>
	</c:forEach>
	</table>
	<a href="addvolunteer"> Add a Volunteer</a>
	<a href="logout.html"> Log Out</a>
</body>
</html>