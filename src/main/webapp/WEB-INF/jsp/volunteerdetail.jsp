<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml11.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Volunteer Detail</title>
</head>
<body>
<form action="../volunteers/${volunteer.id}" method="post">
    <table>
        <tr>
            <td>First Name: </td>
            <td><input type="text" name="firstName" value="${volunteer.firstName}"/> </td>
        </tr>
        <tr>
            <td>Last Name:</td>
            <td><input type="text" name="lastName" value="${volunteer.lastName}"/> </td>
        </tr>
    </table>
    <input type="submit" value="update"/>
</form>
<h3>Total hours spend: "${totalHours}"</h3>
<h2>Skill List</h2>
    <table>
        <c:forEach var="skill" items="${volunteer.skills}">
            <tr>
                <td>${skill.name}</td>
                <td>${skill.description}</td>
                <td>
                	<form method="post" action="../volunteer/skill/remove?volunteerId=${volunteer.id}&&skillId=${skill.id}">
                		<input type="submit" value="Remove" />
                	</form>
                </td>
            </tr>
        </c:forEach>
    </table>

<table>
    <c:forEach var="skill" items="${skillList}">
        <tr>
            <td>${skill.name}</td>
            <td>${skill.description}</td>
            <td>
                <form method="post" action="../volunteer/skill/add?volunteerId=${volunteer.id}&&skillId=${skill.id}">
                    <input type="submit" value="Add" />
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
<h2>Project Participated</h2>
<c:forEach var="project" items="${volunteer.projects}">
    <tr>
        <td>${project.name}</td>
        <td>${project.description}</td>
        <td>Hours spend: ${volunteer.getTimeSpendInProjects().get(project.getId())}</td>
        <td>
            <form method="post" action="../volunteer/project/remove?volunteerId=${volunteer.id}&&projectId=${project.id}">
                <input type="submit" value="Remove" />
            </form>
            <form method="post" action="../volunteer/project/addHours?volunteerId=${volunteer.id}&&projectId=${project.id}">
                <input type="submit" value="Add 1 hour" />
            </form>
        </td>
    </tr>
</c:forEach>
<c:forEach var="project" items="${projectList}">
    <tr>
        <td>${project.name}</td>
        <td>${project.description}</td>
        <td>
            <form method="post" action="../volunteer/project/add?volunteerId=${volunteer.id}&&projectId=${project.id}">
                <input type="submit" value="Add" />
            </form>
        </td>
    </tr>
</c:forEach>
<h2>Had Been Reviewed</h2>
<c:forEach var="review" items="${volunteer.reviews}">
    <tr>
        <td>${review.content}</td>
        <td>${review.date}</td>
        <td>
            <form method="post" action="../volunteer/review/remove?volunteerId=${volunteer.id}&&reviewId=${review.id}">
                <input type="submit" value="Remove" />
            </form>
        </td>
    </tr>
</c:forEach>
<a href="/volunteer/volunteerlist"> Volunteer List</a>
</body>
</html>