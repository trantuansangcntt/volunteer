package cs544.project.volunteer.controller;

import cs544.project.volunteer.domain.Project;
import cs544.project.volunteer.domain.Review;
import cs544.project.volunteer.domain.Skill;
import cs544.project.volunteer.domain.Volunteer;
import cs544.project.volunteer.repository.ProjectRepository;
import cs544.project.volunteer.repository.ReviewRepository;
import cs544.project.volunteer.repository.SkillRepository;
import cs544.project.volunteer.repository.VolunteerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import javax.annotation.Resource;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/volunteer")
public class VolunteerController {

    @Autowired
    private EntityManager em;
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private VolunteerRepository volunteerRepository;

//    @RequestMapping("/")
//    private String addReview(){
//        return "volunteers";
//    }
//

    @RequestMapping("/")
    public String redirectRoot() {
        return "redirect:/volunteers";
    }

    @RequestMapping(value = "/volunteerlist", method = RequestMethod.GET)
    public String getAll(Model model) {
        model.addAttribute("volunteers", volunteerRepository.findAll());
        return "volunteerlist";
    }

    @RequestMapping(value = "addvolunteer")
    public String addAVolunteer(Volunteer volunteer) {
//        volunteerRepository.save(volunteer);
        return "addvolunteer";
    }

    @RequestMapping(value = "/volunteerlist", method = RequestMethod.POST)
    public String add(@RequestParam("lastName") String lastName,@RequestParam("firstName") String firstName, Volunteer volunteer) {
        volunteerRepository.save(new Volunteer(firstName,lastName));
        return "redirect:/volunteer/volunteerlist";
    }

    @RequestMapping(value = "/volunteers/{id}", method = RequestMethod.GET)
    public String get(@PathVariable int id, Model model) {
    	Volunteer tempVol = volunteerRepository.findOne(id);
        List<Skill> skillList = skillRepository.findAll();
        model.addAttribute("volunteer", tempVol);
    	for (Skill s: tempVol.getSkills()) {
    		for (Skill remainSk : skillList)
    		if (s.getId() == remainSk.getId()) {
                skillList.remove(s);
    			break;
    		}
    	}
        model.addAttribute("skillList", skillList);
        
        
        List<Project> projectList = projectRepository.findAll();
        for (Project joinedProject: tempVol.getProjects()){
            for (Project p: projectList){
                if (p.getId() == joinedProject.getId()){
                    projectList.remove(p);
                    break;
                }
            }
        }
        model.addAttribute("projectList", projectList);

        int totalHours = 0;
        for (Project joinedProject: tempVol.getProjects()){
            totalHours += tempVol.getTimeSpendInProjects().get(joinedProject.getId());
        }
        model.addAttribute("totalHours", totalHours);
        return "volunteerdetail";
    }

    @RequestMapping(value = "/volunteers/{id}", method = RequestMethod.POST)
    public String update(Volunteer volunteer, @PathVariable int id) {
        Volunteer tempVol = volunteerRepository.findOne(id);
        tempVol.setLastName(volunteer.getLastName());
        tempVol.setFirstName(volunteer.getFirstName());
        volunteerRepository.save(tempVol); // book.id already set by binding
        return "redirect:/volunteer/volunteerlist";
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String removeVolunteer(Volunteer volunteer, @RequestParam("volunteerId") int volunteerId) {
        Volunteer tempVol = volunteerRepository.findOne(volunteerId);
        volunteerRepository.delete(tempVol);
        return "redirect:/volunteer/volunteerlist";
    }

    @RequestMapping(value = "/volunteer/skill/remove", method = RequestMethod.POST)
    public String removeSkill(Volunteer volunteer, @RequestParam("volunteerId") int volunteerId, @RequestParam("skillId") int skillId) {
    	Volunteer tempVol = volunteerRepository.findOne(volunteerId);
    	for (Skill s: tempVol.getSkills()) {
    		if (s.getId() == skillId) {
    			tempVol.getSkills().remove(s);
    			break;
    		}
    	}
    	volunteerRepository.save(tempVol);
        return "redirect:/volunteer/volunteers/" + volunteerId;
    }

    @RequestMapping(value = "/volunteer/skill/add", method = RequestMethod.POST)
    public String addSkill(Volunteer volunteer, @RequestParam("volunteerId") int volunteerId, @RequestParam("skillId") int skillId) {
        Volunteer tempVol = volunteerRepository.findOne(volunteerId);
       Skill skill= skillRepository.findOne(skillId);
        tempVol.getSkills().add(skill);
        volunteerRepository.save(tempVol);
        return "redirect:/volunteer/volunteers/" + volunteerId;
    }

    @RequestMapping(value = "/volunteer/project/addHours", method = RequestMethod.POST)
    public String add1HourProject(Volunteer volunteer, @RequestParam("volunteerId") int volunteerId, @RequestParam("projectId") int projectId) {
        Volunteer tempVol = volunteerRepository.findOne(volunteerId);
        Project temppro = projectRepository.findOne(projectId);
        int hours = tempVol.getTimeSpendInProjects().get(temppro.getId());
        tempVol.getTimeSpendInProjects().put(temppro.getId(),hours+1);
        volunteerRepository.save(tempVol);
        return "redirect:/volunteer/volunteers/" + volunteerId;
    }

    //volunteer/project/remove
    @RequestMapping(value = "/volunteer/project/remove", method = RequestMethod.POST)
    public String removeProject(Volunteer volunteer, @RequestParam("volunteerId") int volunteerId, @RequestParam("projectId") int projectId) {
        Volunteer tempVol = volunteerRepository.findOne(volunteerId);
        Project temppro = projectRepository.findOne(projectId);
        for (Project p: tempVol.getProjects()) {
            if (p.getId() == projectId) {
                tempVol.getProjects().remove(p);
                tempVol.getTimeSpendInProjects().remove(p.getId());
                break;
            }
        }
        temppro.getVolonteers().remove(tempVol);
        volunteerRepository.save(tempVol);
        projectRepository.save(temppro);
        return "redirect:/volunteer/volunteers/" + volunteerId;
    }
    @RequestMapping(value = "/volunteer/project/add", method = RequestMethod.POST)
    public String addProject(Volunteer volunteer, @RequestParam("volunteerId") int volunteerId, @RequestParam("projectId") int projectId) {
        Volunteer tempVol = volunteerRepository.findOne(volunteerId);
        Project tempPro= projectRepository.findOne(projectId);
//        tempVol.getProjects().add(tempPro);
//        volunteerRepository.save(tempVol);
        tempPro.getVolonteers().add(tempVol);
        tempVol.getTimeSpendInProjects().put(tempPro.getId(),0);
        projectRepository.save(tempPro);
        volunteerRepository.save(tempVol);
        return "redirect:/volunteer/volunteers/" + volunteerId;
    }
    //volunteer/review/remove
    @RequestMapping(value = "/volunteer/review/remove", method = RequestMethod.POST)
    public String removeReview(Volunteer volunteer, @RequestParam("volunteerId") int volunteerId, @RequestParam("reviewId") int reviewId) {
        Volunteer tempVol = volunteerRepository.findOne(volunteerId);
        Review review = reviewRepository.findOne(reviewId);
        tempVol.getReviews().remove(review);
        volunteerRepository.save(tempVol);
        review.setVolunteer(null);
        reviewRepository.save(review);
        return "redirect:/volunteer/volunteers/" + volunteerId;
    }

}
