package cs544.project.volunteer.controller;

import cs544.project.volunteer.service.GenerateSampleDATA;
import org.hibernate.annotations.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/sample")
public class SampleDataController {
	@Autowired
	private GenerateSampleDATA temp;
    @RequestMapping("/addVolunteer")
    private String addReview(){
    	
    	temp.addSampleVolunteer();
        return "index";
    }

    @RequestMapping("/generateSampleData")
    private String sampleData(){

        temp.addSampleData();
        return "index";
    }

}
