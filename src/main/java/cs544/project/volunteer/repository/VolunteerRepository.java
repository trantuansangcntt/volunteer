package cs544.project.volunteer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cs544.project.volunteer.domain.Volunteer;

public interface VolunteerRepository extends JpaRepository<Volunteer, Integer> {
}
