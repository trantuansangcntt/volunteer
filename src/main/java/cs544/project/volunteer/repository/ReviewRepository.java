package cs544.project.volunteer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cs544.project.volunteer.domain.Review;

public interface ReviewRepository extends JpaRepository<Review,Integer> {
}
