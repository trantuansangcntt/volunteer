package cs544.project.volunteer.repository;

import cs544.project.volunteer.domain.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillRepository extends JpaRepository<Skill,Integer> {
}
