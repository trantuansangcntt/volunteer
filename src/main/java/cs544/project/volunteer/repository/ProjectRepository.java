package cs544.project.volunteer.repository;

import cs544.project.volunteer.domain.Project;
import cs544.project.volunteer.domain.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Integer> {
//    public void addSkill(Skill skill);
}
