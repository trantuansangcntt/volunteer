package cs544.project.volunteer.service;

import cs544.project.volunteer.domain.Review;
import cs544.project.volunteer.repository.ReviewRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ReviewService {
    @Autowired
    private ReviewRepository reviewRepository;

    public Review save(Review review) {
        return reviewRepository.save(review);
    }

    public List<Review> findAll() {
        return reviewRepository.findAll();
    }

    public Review getReview(int id) {
        return reviewRepository.findOne(id);
    }

    public void delete(Review review) {
        reviewRepository.delete(review);
    }
}
