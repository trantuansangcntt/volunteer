package cs544.project.volunteer.service;

import cs544.project.volunteer.domain.*;
import cs544.project.volunteer.repository.ProjectRepository;
import cs544.project.volunteer.repository.ReviewRepository;
import cs544.project.volunteer.repository.SkillRepository;
import cs544.project.volunteer.repository.VolunteerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service

public class GenerateSampleDATA {

    @Autowired
    private  EntityManager em;
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private VolunteerRepository volunteerRepository;


    public void addSampleData(){
        addSampleVolunteer();
        addProjectSample();
        addSkillSample();
        addReviewSample();
        addSkillToProject();
        addSkillToVolunteer();
        addVolunteerToProject();
        addReviewToProjectOrVolunteer();
    }

    public void addSkillSample(){
        List<Integer> list = Arrays.asList(1, 2, 3);
        list.forEach(i -> {
            addSkillSample(i);
        });
    }

    public void addProjectSample(){
        List<Integer> list = Arrays.asList(1, 2, 3);
        list.forEach(i -> {
            addProjectSample(i);
        });

    }

    public void addProjectSample(int i){

        Project project = new Project();
        project.setName("Project name "+i);
        project.setDescription("Project Description "+i);
        project.setLocation("Project Location "+i);
        project.setTotalHours((int)(Math.random()*80)+40);
        project.setStartDate(new Date());
        project.setEndDate(new Date());
        project.setStatus(Status.OPEN);
        projectRepository.save(project);
    }


    public void addSkillSample(int i) {
        Skill skill = new Skill(); 
        skill.setName("Skill "+i);
        skill.setDescription("Description "+i);
        skillRepository.save(skill);
    }

    public void addSampleVolunteer(){
//    	Volunteer lastVol = volunteerRepository.findOne(3);
//    	System.out.println(lastVol);
//        addVolunteer("FirstName1","LastName1");
        List<Integer> list = Arrays.asList(1, 2, 3);
        list.forEach(i -> {
            addSampleVolunteer(i);
        });
//        addVolunteer("FirstName3","LastName3");
//        addVolunteer("FirstName4","LastName4");
//        addVolunteer("FirstName5","LastName5");
//        addVolunteer("FirstName6","LastName6");
    }

    public void addSampleVolunteer(int i) {
        Volunteer v1 = new Volunteer("First Name "+i,"Last Name "+i);
        volunteerRepository.save(v1);
    }

        @Transactional
    public void addVolunteer(String firstName, String lastName){
        Volunteer v1 = new Volunteer(firstName,lastName);
        volunteerRepository.save(v1);
    }

    public void addReviewSample(){
        List<Integer> list = Arrays.asList(1,2,3);
        list.forEach(i ->{
            addReviewSample(i);
        });
    }

    private void addReviewSample(Integer i) {
        Review review = new Review();
        review.setContent("Content "+i);
        review.setDate(new Date());
        review.setGrading(Grading.EXPECTED);
        reviewRepository.save(review);
    }

    @Transactional
    public void addSkillToProject(){
        List<Project> projects = projectRepository.findAll();
        List<Skill> skills = skillRepository.findAll();
        int i = 0;
        for (Project p :
                projects) {
//            if (p.getSkillNeeded())
            p.getSkillNeeded().add(skills.get(i));
            i++;
            projectRepository.save(p);
        }
    }

    public void addSkillToVolunteer(){
        List<Volunteer> volunteers = volunteerRepository.findAll();
        List<Skill> skills = skillRepository.findAll();
        int count = skills.size();
        int i = 1;
        for (Volunteer v :
                volunteers) {
            v.getSkills().add(skills.get(count-i));
            volunteerRepository.save(v);
            i++;
            if (i > count) i = 1;
        }
    }

    public void addVolunteerToProject(){
        List<Project> projects = projectRepository.findAll();
        List<Volunteer> volunteers = volunteerRepository.findAll();
        int count = volunteers.size();
//        int i = 1;
        for (Project p :
                projects) {
            for (Skill sk :
                    p.getSkillNeeded()) {
                for (Volunteer vl :
                        volunteers) {
                    List<Skill> vlSkill = vl.getSkills();
                    if (vlSkill.contains(sk)) {
                        p.getVolonteers().add(vl);
                        vl.getTimeSpendInProjects().put(p.getId(),20);
                        projectRepository.save(p);
                    }
                }
            }
        }
    }

    public void addReviewToProjectOrVolunteer(){
        List<Project> projects = projectRepository.findAll();
        List<Volunteer> volunteers = volunteerRepository.findAll();
        List<Review> reviews = reviewRepository.findAll();
        int sizeOfProject = projects.size();
        int sizeOfVolunteer = volunteers.size();

        int n = 1;
        for (Project p :
                projects) {
            Review temp = generateAReview(n);
            temp.setProject(p);
            reviewRepository.save(temp);
            n++;
        }
        for (Volunteer vl :
                volunteers) {
            Review temp = generateAReview(n);
            temp.setVolunteer(vl);
            reviewRepository.save(temp);
            n++;
        }
        for (Volunteer vl :
                volunteers) {
            Review temp = generateAReview(n);
            for (Project p :
                    projects) {
                temp.setVolunteer(vl);
                temp.setProject(p);
                reviewRepository.save(temp);
                n++;
            }
        }

    }

    public Review generateAReview(int n){
        Review review = new Review();
        if(n%2==0) review.setGrading(Grading.EXPECTED);
        else review.setGrading(Grading.NEED_IMPROVEMENT);
        review.setDate(new Date());
        review.setContent("Content abs "+n);
        return review;
    }

}
