package cs544.project.volunteer.domain;

import java.util.List;

public interface IReviewDao {
    public abstract List<Review> getAll();

    public abstract void add(Review Review);

    public abstract Review get(int id);

    public abstract void update(int ReviewId, Review Review);

    public abstract void delete(int ReviewId);

}
