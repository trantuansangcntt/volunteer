package cs544.project.volunteer.domain;

import javax.persistence.Embeddable;

@Embeddable
public enum Status {
    OPEN, 
    CLOSED
}
