package cs544.project.volunteer.domain;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="review")
public class Review {
    @Id
    @GeneratedValue
    private int id;
    private String content;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @ManyToOne
    @JoinColumn(name="volunteer_id_fk")
    private Volunteer volunteer;

    @ManyToOne
    @JoinColumn(name="project_id_fk")
    private Project project;

    @Enumerated(EnumType.STRING)
    private Grading grading;



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Volunteer getVolunteer() {
		return volunteer;
	}
	public void setVolunteer(Volunteer volunteer) {
		this.volunteer = volunteer;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public Grading getGrading() {
		return grading;
	}
	public void setGrading(Grading grading) {
		this.grading = grading;
	}
	@Override
	public String toString() {
		return "Review [id=" + id + ", content=" + content + ", date=" + date + ", volunteer=" + volunteer
				+ ", project=" + project + ", grading=" + grading + "]";
	}
    
}
