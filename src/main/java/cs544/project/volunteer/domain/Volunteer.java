package cs544.project.volunteer.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
public class Volunteer {
    @Id @GeneratedValue
    private int id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;

    @ManyToMany
    @JoinTable(name="volunteer_skill",
            joinColumns=@JoinColumn(name="volunteer_id"),
            inverseJoinColumns=@JoinColumn(name="skill_id"))
    private List<Skill> skills= new ArrayList<Skill>();

    @OneToMany(mappedBy="volunteer")
    private List<Review> reviews = new ArrayList<Review>();

    @ManyToMany(mappedBy="volonteers")
    private List<Project> projects = new ArrayList<Project>();

    @ElementCollection
    private Map<Integer, Integer> timeSpendInProjects = new HashMap<Integer, Integer>();



    public Volunteer() {
    	
    }

    public void setTimeSpendInProjects(Map<Integer, Integer> timeSpendInProjects) { this.timeSpendInProjects = timeSpendInProjects; }
    public Map<Integer, Integer> getTimeSpendInProjects() { return timeSpendInProjects; }


    public void setId(int id) { this.id = id; }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public Volunteer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public List<Project> getProjects() {
        return projects;
    }

    @Override
    public String toString() {
        return "Volunteer: "+firstName+" "+lastName;
    }
}
